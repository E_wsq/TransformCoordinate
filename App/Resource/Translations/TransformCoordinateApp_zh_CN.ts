<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.ui" line="14"/>
        <source>Coordinate conversion</source>
        <translation>坐标转换</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="25"/>
        <source>Source coordinate:</source>
        <translation>源坐标：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="37"/>
        <location filename="../../mainwindow.ui" line="76"/>
        <source>Latitude:</source>
        <translation>纬度：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="44"/>
        <location filename="../../mainwindow.ui" line="69"/>
        <source>Longitude:</source>
        <translation>经度：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="57"/>
        <source>Destination coordinate:</source>
        <translation>目标坐标：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="112"/>
        <location filename="../../mainwindow.ui" line="186"/>
        <location filename="../../mainwindow.ui" line="261"/>
        <source>Conversion</source>
        <translation>转换</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="123"/>
        <source>Transform File</source>
        <translation>转换文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="131"/>
        <source>Source File:</source>
        <translation>源文件：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="141"/>
        <location filename="../../mainwindow.ui" line="162"/>
        <location filename="../../mainwindow.ui" line="216"/>
        <location filename="../../mainwindow.ui" line="237"/>
        <source>Browse</source>
        <translation>浏览</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="152"/>
        <source>Destination File:</source>
        <translation>目标文件：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="198"/>
        <source>Transform files in path</source>
        <translation>转换文件夹中的文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="206"/>
        <source>Source Directory:</source>
        <translation>源目录：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="227"/>
        <source>Destination Directory:</source>
        <translation>目标目录：</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="283"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../../mainwindow.ui" line="292"/>
        <source>About(&amp;A)</source>
        <translation>关于(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="59"/>
        <source>Open source file</source>
        <translation>打开源文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="58"/>
        <source>GPX file(*.gpx);;NMea file(*.nmea);;ACT file(*.act);;txt(*.txt);;All files(*.*)</source>
        <translation>GPX file(*.gpx);;NMea file(*.nmea);;ACT file(*.act);;txt(*.txt);;所有文件(*.*)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="71"/>
        <source>Open Destination file</source>
        <translation>打开目标文件</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="67"/>
        <source>GPX file(*.gpx);;</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="69"/>
        <source>KML file(*.kml)</source>
        <translation>KML 文件(*.kml)</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="80"/>
        <location filename="../../mainwindow.cpp" line="140"/>
        <source>Start transform ......</source>
        <translation>开始转换……</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="89"/>
        <location filename="../../mainwindow.cpp" line="160"/>
        <source>Ready</source>
        <translation>预备</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="108"/>
        <source>Open source directory</source>
        <translation>打开源目录</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="118"/>
        <source>Open destination directory</source>
        <translation>打开目标目录</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="149"/>
        <source>Be transforming </source>
        <translation>正在转换 </translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../main.cpp" line="19"/>
        <source>Transform coordinate</source>
        <translation>坐标转换</translation>
    </message>
</context>
</TS>
